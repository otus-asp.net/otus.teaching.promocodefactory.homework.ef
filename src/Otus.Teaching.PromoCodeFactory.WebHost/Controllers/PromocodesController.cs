﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly ICustomerRepository _customerRepository;

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        ///
        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Preference> preference, ICustomerRepository customer)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preference;
            _customerRepository = customer;
        }
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();

            var promocodeModelList = promocodes.Select(x =>
               new PromoCodeShortResponse()
               {
                   Id = x.Id,
                   Code = x.Code,
                   ServiceInfo = x.ServiceInfo,
                   BeginDate = x.BeginDate.ToString(),
                   EndDate = x.EndDate.ToString(),
                   PartnerName = x.PartnerName
               }).ToList();

            return promocodeModelList;

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            var newPromoCode = new PromoCode()
            {
                Id = new Guid(),
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                BeginDate = new DateTime(),
                EndDate = new DateTime(),

            };
            var preferences = await _preferenceRepository.GetAllAsync();
            var pref = preferences.FirstOrDefault(p => p.Name == request.Preference);
            newPromoCode.Preference = pref;

            var allCustomers = await _customerRepository.GetAllWithPreferencesAsync();
            var customerWithPreference = allCustomers.Where(c => c.Preferences.Any(p => p.Preference == pref)).ToList();

            if (customerWithPreference != null)
            {
                customerWithPreference.ForEach(c => c.PromoCodes.Add(newPromoCode));
            }

            await _promocodeRepository.AddAsync(newPromoCode);
            return NoContent();

        }
    }
}
