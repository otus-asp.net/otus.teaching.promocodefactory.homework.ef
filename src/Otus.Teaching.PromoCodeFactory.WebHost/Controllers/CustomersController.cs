﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить данныей всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerModelList = customers.Select(x =>
            new CustomerShortResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToList();

            return customerModelList;
        }
        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);

            return Ok(response);
        }
        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {

            var customer = new Customer()
            {
                Id = new Guid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };

            if (request.PreferenceIds != null)
            {
                customer.Preferences = request.PreferenceIds.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    PreferenceId = x
                }).ToList();
            }

            await _customerRepository.AddAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {

            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            if (request.PreferenceIds != null)
            {
                customer.Preferences?.Clear();
                customer.Preferences = request.PreferenceIds.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    PreferenceId = x
                }).ToList();
            }
            await _customerRepository.UpdateAsync(customer);

            return Ok();

        }
        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            await _customerRepository.DeleteAsync(customer);

            return Ok();

        }
    }
}