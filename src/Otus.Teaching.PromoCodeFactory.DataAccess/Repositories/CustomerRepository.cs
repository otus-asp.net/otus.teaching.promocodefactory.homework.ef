﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        private readonly DataContext _dataContext;
        public CustomerRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {

            return await _dataContext.Customers
                .Include(x => x.Preferences)
                .ThenInclude(x => x.Preference)
                .Include(x => x.PromoCodes)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(Customer Entity)
        {
            var preferences = _dataContext.PromoCodes.Where(p => p.CustomerId == Entity.Id).ToList();
            _dataContext.RemoveRange(preferences);
            _dataContext.Customers.Remove(Entity);
            _dataContext.SaveChanges();
        }

        public async Task<Customer> UpdateAsync(Customer entity)
        {
            _dataContext.Customers.Update(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<Customer>> GetAllWithPreferencesAsync()
        {
            return await _dataContext.Customers
                .Include(x => x.Preferences)
                .ThenInclude(x => x.Preference)
                .Include(x => x.PromoCodes).ToListAsync();
        }
    }
}

