﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataContext _context;
        internal DbSet<T> Data;

        public EfRepository(DataContext context)
        {
            _context = context;
            Data = _context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Data.ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await Data.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> AddAsync(T obj)
        {
            Data.Add(obj);
            _context.SaveChanges();
            return await Data.FirstOrDefaultAsync(x => x.Id == obj.Id);

        }
    }
}

