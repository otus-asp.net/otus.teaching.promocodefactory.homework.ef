﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {


        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }


        public DataContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);

            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);

            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);

            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);


            modelBuilder.Entity<CustomerPreference>()
                .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);


            modelBuilder.Entity<CustomerPreference>().HasData(new CustomerPreference { CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") });

            modelBuilder.Entity<CustomerPreference>().HasData(new CustomerPreference { CustomerId = Guid.Parse("A6C8C6B1-4349-45B0-AB31-244740AAF0A2"), PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") });





            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseSqlite("Data source=PromoCodeFactory.db", b => b.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.WebHost"));

            //optionsBuilder.EnableSensitiveDataLogging();

        }
    }
}

